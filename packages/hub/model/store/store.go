package store

func New() {

}

type Store struct {
}

func (s *Store) CreateSnippet() {

}

type Store interface {
	CreateSnippet
	UpdateSnippet
	DeleteSnippet
	GetSnippets

	CreateTemplate

	CreateTheme

	CreateUser

	CreateWidgets
}
