package config

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/spf13/viper"
	"github.com/teacat/pathx"
	"github.com/tocas-ui/tocas-ui/cli/path"
)

const (
	// KeyLocale 是語系設定的鍵名。
	KeyLocale = "locale"
	// KeySassCompiler 是 Sass 編譯器的鍵名。
	KeySassCompiler = "sassCompiler"
)

const (
	// CompilerSass 是 Dart Sass 編譯器。
	CompilerSass = iota
	// CompilerWellington 是 Golang Sass 編譯器。
	CompilerWellington
	// CompilerNode 是 Node Sass 編譯器。
	CompilerNode
	// CompilerSassc 是 C Sass 編譯器。
	CompilerSassc
)

// defaultConfig 是預設的設定檔案內容。
var defaultConfig = `
sassCompiler: 1
locale: zh-tw
`

// Init 會初始化設定系統。
func Init() {
	// 如果設定檔案並不存在，那麼就以預設內容建立設定檔案。
	if !pathx.Exists(path.PathCliConfig) {
		err := ioutil.WriteFile(path.PathCliConfig, []byte(defaultConfig), os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
	// 透過 viper 讀取設定檔。
	viper.SetConfigName("config")
	viper.AddConfigPath(filepath.Dir(path.PathCliConfig))
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

// Set 會更改目前的設定並寫入至設定檔案中保存。
func Set(key string, value interface{}) {
	viper.Set(key, value)
	err := viper.WriteConfig()
	if err != nil {
		panic(err)
	}
}

// String 會以字串型態取得指定設定。
func String(key string) string {
	return viper.GetString(key)
}

// Int 會以正整數型態取得指定設定。
func Int(key string) int {
	return viper.GetInt(key)
}
