package interact

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/tocas-ui/tocas-ui/cli/docs"

	"github.com/teacat/noire"
	"github.com/teacat/pathx"
	"github.com/tocas-ui/tocas-ui/cli/config"
	"github.com/tocas-ui/tocas-ui/cli/fontawesome"
	"github.com/tocas-ui/tocas-ui/cli/i18n"
	"github.com/tocas-ui/tocas-ui/cli/path"
	"github.com/tocas-ui/tocas-ui/cli/watcher"
)

func Start() {
	switch ask(i18n.M("Hello"), i18n.O("Install"), i18n.O("Developer"), "Change Language", i18n.O("Exit")) {
	// 安裝 Tocas UI
	case 0:
		interactInstallation()
		break
	// 開發人員選項
	case 1:
		interactDeveloper()
		break
	// Change Language
	case 2:
		interactLanguage()
		break
	// 離開
	case 3, -1:
		os.Exit(0)
		break
	}
}

func interactDeveloper() {
	switch ask(i18n.M("Developer"), i18n.O("Watch"), i18n.O("Compiler"), i18n.O("FontAwesome"), i18n.O("Docs"), i18n.O("PreSetting"), i18n.O("DocsMarkdown"), i18n.O("AutoColor"), i18n.O("Back")) {
	// 監聽並即時編譯原始碼
	case 0:
		watcher.Start()
		break
	// 更改編譯器
	case 1:
		interactCompiler()
		break
	// 更新 FontAwesome
	case 2:
		fontawesome.Start()
		break
	// 編譯所有語言文件
	case 3:
		interactDocsAll()
		break
	// 更改預載設定
	case 4:
		interactPreSetting()
		break
	// 將所有文件編譯成 Markdown
	case 5:
		interactDocsAllMarkdown()
		break
	// 輸出自動調色盤
	case 6:
		interactAutoColor()
		break
	// 返回
	case 7, -1:
		Start()
		break
	}
}

func interactAutoColor() {
	//
	a := askInput(i18n.Q("AutoColor"), pathx.Getwd())
	c := noire.NewHex(a)
	//
	for i := 0.7; i >= 0; i -= 0.15 {
		fmt.Println(c.Tint(i).HTML())
	}
	fmt.Println(c.HTML())
	//
	for i := 0.1; i <= 0.7; i += 0.15 {
		fmt.Println(c.Shade(i).HTML())
	}

}

func interactDocsAll() {
	switch askConfirm("只編譯你語系的文件嗎？") {
	// Y
	case true:
		files, err := filepath.Glob(pathx.Join(path.PathDocsTranslations, i18n.Locale(), "*"))
		if err != nil {
			panic(err)
		}
		for _, v := range files {
			fs, err := filepath.Glob(pathx.Join(v, "*"))
			if err != nil {
				panic(err)
			}
			for _, j := range fs {
				docs.Compile(j)
			}
		}
		break
	// N
	case false:
		askMulti("你想要編譯哪些語系文件？", "zh-TW")
		break
	}
}

func interactDocsAllMarkdown() {

}

func interactPreSetting() {
	switch ask(i18n.Q("PreSetting"), i18n.O("PreComponent"), i18n.O("PreTheme"), i18n.O("PreWidget"), i18n.O("Restore"), i18n.O("Back")) {
	// 元件
	case 0:

		break
	// 主題
	case 1:

		break
	// 附屬元件
	case 2:

		break
	// 將所有設定還原至預設
	case 3:

		break
	// 返回
	case 4, -1:
		interactDeveloper()
		break
	}
}

func interactCompiler() {
	switch ask(i18n.Q("Compiler"), i18n.O("Sass"), i18n.O("Back")) {
	// Sass
	case 0:
		switch ask(i18n.Q("Sass"), i18n.O("Sassc"), i18n.O("Wellington"), i18n.O("Node"), i18n.O("Sass")) {
		// Sassc（快速、C/C++）
		case 0:
			config.Set(config.KeySassCompiler, config.CompilerSassc)
			interactCompiler()
			break
		// Wellington（快速、Golang）
		case 1:
			config.Set(config.KeySassCompiler, config.CompilerWellington)
			interactCompiler()
			break
		// Node Sass（稍慢、Node.js）
		case 2:
			config.Set(config.KeySassCompiler, config.CompilerNode)
			interactCompiler()
			break
		// Sass（最慢、Dart）
		case 3:
			config.Set(config.KeySassCompiler, config.CompilerSass)
			interactCompiler()
			break
		}
		break
	// 返回
	case 1:
		interactDeveloper()
		break
	}
}

func interactInstallation() {
	switch askConfirm(i18n.Q("Custom")) {
	// Y
	case true:
		askMulti(i18n.Q("Select"),
			i18n.O("Acccordion"),
			i18n.O("Breadcrumb"),
			i18n.O("Button"),
			i18n.O("Calendar"),
			i18n.O("Card"),
			i18n.O("Carousel"),
			i18n.O("Checkbox"),
			i18n.O("Color"),
			i18n.O("Comment"),
			i18n.O("Comparison"),
			i18n.O("Container"),
			i18n.O("Conversation"),
			i18n.O("Dimmer"),
			i18n.O("Divider"),
			i18n.O("Dropdown"),
			i18n.O("Embed"),
			i18n.O("Feed"),
			i18n.O("Flag"),
			i18n.O("Form"),
			i18n.O("Grid"),
			i18n.O("Header"),
			i18n.O("Icon"),
			i18n.O("Image"),
			i18n.O("Input"),
			i18n.O("Items"),
			i18n.O("Label"),
			i18n.O("List"),
			i18n.O("Loader"),
			i18n.O("Menu"),
			i18n.O("Message"),
			i18n.O("Modal"),
			i18n.O("Panes"),
			i18n.O("Placeholder"),
			i18n.O("Popup"),
			i18n.O("Progress"),
			i18n.O("Quote"),
			i18n.O("Rating"),
			i18n.O("Search"),
			i18n.O("Segment"),
			i18n.O("Sidebar"),
			i18n.O("Slate"),
			i18n.O("Slider"),
			i18n.O("Snackbar"),
			i18n.O("Sortable"),
			i18n.O("Statistic"),
			i18n.O("Steps"),
			i18n.O("Sticky"),
			i18n.O("Tab"),
			i18n.O("Table"),
			i18n.O("Topic"),
			i18n.O("Transfer"),
			i18n.O("Transition"),
			i18n.O("Validation"),
			i18n.O("Window"))
		break
		// N
	case false:
		break
	}

	//
	askInput(i18n.Q("Project"), pathx.Getwd())

	//
	askInput(i18n.Q("Tocas"), "/tocas")

	//
	switch askConfirm(i18n.Q("CDN")) {
	// N
	case false:
		break
	// Y
	case true:
		break
	}

	//
	switch askConfirm(i18n.Q("Compress")) {
	// N
	case false:
		break
	// Y
	case true:
		break
	}
}

func interactLanguage() {
	options := []string{i18n.O("Back")}
	locales := []string{""}
	for k, v := range i18n.Locales() {
		locales = append(locales, k)
		options = append(options, v)
	}
	ans := ask("Choose your language", options...)
	switch ans {
	case 0, -1:
		Start()
		return
	}
	i18n.SetLocale(locales[ans])
	Start()
}
