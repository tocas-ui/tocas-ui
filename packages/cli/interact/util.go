package interact

import survey "gopkg.in/AlecAivazis/survey.v1"

type answer struct {
	options []string
	answer  int
}

func (a *answer) WriteAnswer(field string, value interface{}) error {
	for k, v := range a.options {
		if v == value.(string) {
			a.answer = k
		}
	}
	return nil
}

func ask(message string, options ...string) int {
	a := &answer{options: options, answer: -1}
	prompt := &survey.Select{
		Message: message,
		Options: options,
	}
	survey.AskOne(prompt, a, nil)
	return a.answer
}

func askInput(message string, defaultAnswer string) (answer string) {
	answer = defaultAnswer
	prompt := &survey.Input{
		Message: message,
		Default: defaultAnswer,
	}
	survey.AskOne(prompt, &answer, nil)
	return
}

type answers struct {
	options []string
	answers []int
}

func (a *answers) WriteAnswer(field string, value interface{}) error {
	for k, v := range a.options {
		for _, j := range value.([]string) {
			if v == j {
				a.answers = append(a.answers, k)
			}
		}
	}
	return nil
}

func askMulti(message string, options ...string) []int {
	a := &answers{options: options}
	prompt := &survey.MultiSelect{
		Message: message,
		Options: options,
	}
	survey.AskOne(prompt, a, nil)
	return a.answers
}

func askConfirm(message string) bool {
	a := false
	prompt := &survey.Confirm{
		Message: message,
	}
	survey.AskOne(prompt, &a, nil)
	return a
}
