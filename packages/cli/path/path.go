package path

import "github.com/teacat/pathx"

var (
	root = pathx.ExecutableDir()
)

var (
	// components/
	PathComponents = pathx.JoinDir(root, "../", "../", "components")
	// components/src/
	PathComponentsSrc = pathx.JoinDir(PathComponents, "src")
	// components/src/tocas.sass
	PathComponentsSrcTocas = pathx.Join(PathComponentsSrc, "tocas.sass")
	// components/dist/
	PathComponentsDist = pathx.JoinDir(PathComponents, "dist")
	// components/dist/tocas.css
	PathComponentsDistTocas = pathx.Join(PathComponentsDist, "tocas.css")
	// components/dist/fonts/icons
	PathComponentsDistIcons = pathx.JoinDir(PathComponentsDist, "fonts", "icons")
	// components/src/icon/_icons.sass
	PathComponentsIcon = pathx.Join(PathComponentsSrc, "icon", "_icon.sass")

	// examples/
	PathExamples = pathx.JoinDir(root, "../", "../", "examples")
	// hub/
	PathHub = pathx.JoinDir(root, "../", "../", "hub")

	// modules/
	PathModules = pathx.JoinDir(root, "../", "../", "modules")
	// modules/dist
	PathModulesDist = pathx.JoinDir(PathModules, "dist")
	// modules/src
	PathModulesSrc = pathx.JoinDir(PathModules, "src")

	// widgets/
	PathWidgets = pathx.JoinDir(root, "../", "../", "widgets")
	// widgets/dist
	PathWidgetsDist = pathx.JoinDir(PathWidgets, "dist")
	// widgets/src
	PathWidgetsSrc = pathx.JoinDir(PathWidgets, "src")

	// themes/
	PathThemes = pathx.JoinDir(root, "../", "../", "themes")
	// themes/dist
	PathThemesDist = pathx.JoinDir(PathThemes, "dist")
	// themes/src
	PathThemesSrc = pathx.JoinDir(PathThemes, "src")

	// selector/
	PathSelector = pathx.JoinDir(root, "../", "../", "selector")

	// docs/
	PathDocs = pathx.JoinDir(root, "../", "../", "docs")
	// docs/dist/
	PathDocsDist = pathx.JoinDir(PathDocs, "dist")
	// docs/assets/ ERR: RELATIVE TO DOCS
	PathDocsAssets = pathx.JoinDir(PathDocsDist, "assets")
	// docs/templates/
	PathDocsTemplates = pathx.JoinDir(PathDocs, "templates")
	// docs/translations/
	PathDocsTranslations = pathx.JoinDir(PathDocs, "translations")

	// cli/translations
	PathCliTranslations = pathx.JoinDir(root, "../", "translations")
	// cli/config/config.yml
	PathCliConfig = pathx.Join(root, "../", "config", "config.yml")
)
