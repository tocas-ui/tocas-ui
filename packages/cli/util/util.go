package util

import (
	"path/filepath"
	"regexp"

	"github.com/teacat/pathx"
	"github.com/tocas-ui/tocas-ui/cli/path"
)

func ReplaceAllStringSubmatchFunc(re *regexp.Regexp, str string, repl func([]string) string) string {
	result := ""
	lastIndex := 0

	for _, v := range re.FindAllSubmatchIndex([]byte(str), -1) {
		groups := []string{}
		for i := 0; i < len(v); i += 2 {
			groups = append(groups, str[v[i]:v[i+1]])
		}

		result += str[lastIndex:v[0]] + repl(groups)
		lastIndex = v[1]
	}

	return result + str[lastIndex:]
}

//
func ComponentNames() (components []string) {
	return collectFilenames(path.PathComponentsSrc)
}

//
func WidgetNames() []string {
	return collectFilenames(path.PathWidgetsSrc)
}

//
func ThemeNames() []string {
	return collectFilenames(path.PathThemesSrc)
}

//
func ModuleNames() []string {
	return collectFilenames(path.PathComponents)
}

func collectFilenames(path string) (components []string) {
	files, err := filepath.Glob(pathx.Join(path, "*"))
	if err != nil {
		panic(err)
	}
	for _, v := range files {
		n := pathx.Name(v)
		if n == "globals" || n == "tocas" {
			continue
		}
		components = append(components, n)
	}
	return
}
