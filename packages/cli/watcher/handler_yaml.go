package watcher

import (
	"github.com/tocas-ui/tocas-ui/cli/docs"
)

func yaml(e *event) {
	docs.Compile(e.path)
}
