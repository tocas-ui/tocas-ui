package watcher

import (
	"log"
	"time"

	"github.com/radovskyb/watcher"
	"github.com/teacat/pathx"
	"github.com/tocas-ui/tocas-ui/cli/path"
)

var (
	w *watcher.Watcher
)

func Start() {
	w = watcher.New()
	w.SetMaxEvents(1)
	go watch()
	log.Println("現已開始監聽")
	if err := w.AddRecursive(path.PathComponents); err != nil {
		log.Fatalln(err)
	}
	if err := w.AddRecursive(path.PathDocsTranslations); err != nil {
		log.Fatalln(err)
	}
	if err := w.AddRecursive(path.PathModules); err != nil {
		log.Fatalln(err)
	}
	if err := w.AddRecursive(path.PathSelector); err != nil {
		log.Fatalln(err)
	}
	if err := w.Start(time.Millisecond * 100); err != nil {
		log.Fatalln(err)
	}
}

type event struct {
	name  string
	path  string
	ext   string
	event watcher.Event
	//sourceType sourceType
}

func watch() {
	for {
		select {
		case evt := <-w.Event:
			e := &event{
				name:  pathx.Name(evt.Path),
				path:  evt.Path,
				ext:   pathx.Ext(evt.Path),
				event: evt,
			}
			switch e.ext {
			case "coffee":
				coffee(e)
			case "sass":
				sass(e)
			case "pug":
				pug(e)
			case "yml":
				yaml(e)
			}
		case err := <-w.Error:
			log.Fatalln(err)
		case <-w.Closed:
			return
		}
	}
}

type sourceType int

const (
	sourceTypeComponent sourceType = iota
	sourceTypeComponentTest
	sourceTypeModule
	sourceTypeModuleTest
	sourceTypeDocsTranslation
	sourceTypeSelector
)

func isSource() {

}
