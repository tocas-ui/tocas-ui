package watcher

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/TeaMeow/TocasUI/cli/util"
	"github.com/teacat/pathx"
)

func pug(e *event) {
	// 先透過 Pug 將元件測試檔案編譯為 HTML 檔案。
	executeWithEvent(e, []string{"pug", e.path})

	// 讀取已經轉譯好的 HTML 檔案。
	tmpPath := pathx.Join(filepath.Dir(e.path), e.name+".html")
	dat, err := ioutil.ReadFile(tmpPath)
	if err != nil {
		panic(err)
	}

	// 保存所有章節標記的按鈕 HTML 內容。
	var buttons string
	var jsButtons string

	// 找到所有的章節標記，然後透過 RegExp 取得其中的章節名稱。
	re, err := regexp.Compile(`<!-- \+ (.*?)-->`)
	if err != nil {
		panic(err)
	}
	headers := re.FindAllStringSubmatch(string(dat), -1)
	for _, v := range headers {
		buttons += fmt.Sprintf(`<a href="#%s" class="ts fluid button">%s</a>`, url.QueryEscape(v[1]), v[1])
	}

	//
	re, err = regexp.Compile(`(?s)<!-- \- (.*?)--><script>(.*?)<\/script>`)
	if err != nil {
		panic(err)
	}

	headers = re.FindAllStringSubmatch(string(dat), -1)
	for k, v := range headers {
		jsButtons += fmt.Sprintf(`<script>function action%d(){%s}</script><a href="#!" class="ts fluid button" onclick="action%d()">%s</a>`, k, v[2], k, v[1])
	}

	// 初始化一個新的測試頁面 HTML 內容。
	newContent := fmt.Sprintf(`
	<html>
	<head>
	<title>%s</title>
	<link rel="stylesheet" href="../../dist/tocas.css"><meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="../../../dist/tocas.js"></script>
	<script src="../../../dist/accordion.js"></script>
	<style type="text/css">
	#main {
		position: fixed;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		display: flex;
	}
	#sidebar {
		padding: 16px;
		flex: 0.13;
		overflow-y: scroll;
	}
	#sidebar .ts.button {
		text-align   : left;
		margin-bottom: 8px;
	}
	#content {
		padding: 32px;
		flex: 1;
		overflow-y: scroll;
	}
	</style>
	</head>
	<body>
		<div id="main">
			<div id="sidebar">
				<div class="ts form">
					<fieldset>
						<legend>Core</legend>
						%s
					</fieldset>
					<fieldset>
						<legend>Module</legend>
						%s
					</fieldset>
				</div>
			</div>
			<div id="content">
				%s
			</div>
		</div>
	</body>
	</html>`, e.name, buttons, jsButtons, string(dat))

	//
	re, err = regexp.Compile(`<!-- \+ (.*?)-->`)
	if err != nil {
		panic(err)
	}

	newContent = util.ReplaceAllStringSubmatchFunc(re, newContent, func(groups []string) string {
		return fmt.Sprintf(`<br><br><!-- + %s --><h1 id="%s">%s</h1>`, groups[1], url.QueryEscape(groups[1]), groups[1])
	})

	//
	re, err = regexp.Compile(`(?s)<!-- \- (.*?)--><script>(.*?)<\/script>`)
	if err != nil {
		panic(err)
	}
	newContent = re.ReplaceAllString(newContent, "")

	//
	newContent = strings.Replace(newContent, ">", "> ", -1)
	newContent = strings.Replace(newContent, "<", " <", -1)

	re, err = regexp.Compile(`<a class="([a-zA-Z0-9 -]*)">`)
	if err != nil {
		panic(err)
	}
	newContent = re.ReplaceAllString(newContent, "<a class=\"$1\" href=\"#!\">")

	ioutil.WriteFile(tmpPath, []byte(newContent), 777)
}
