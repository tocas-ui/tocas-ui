package watcher

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

func execute(format string, args ...interface{}) {
	format = fmt.Sprintf(format, args)
	command := strings.Fields(format)
	now := time.Now()
	cmd := exec.Command(command[0], command[1:]...)
	//cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stdout
	if err := cmd.Run(); err != nil {
		panic(err)
	}
	log.Printf("已執行（%s）：%s\n", time.Since(now), format)
}

func executeWithEvent(event *event, command []string) {
	now := time.Now()
	cmd := exec.Command(command[0], command[1:]...)
	//cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stdout
	cmd.Run()
	//if err := cmd.Run(); err != nil {
	//	panic(err)
	//}
	log.Printf("已執行 %s（%s）：%s\n", cmdToName(command[0]), time.Since(now), event.path)
}

func cmdToName(cmd string) string {
	switch cmd {
	case "sass":
		return "Dart Sass"
	case "wt":
		return "Wellington"
	case "node-sass":
		return "Node Sass"
	case "sassc":
		return "Sassc"
	case "pug":
		return "Pug"
	case "coffee":
		return "CoffeeScript"
	}
	return "NONAME"
}

func highlight(input string) string {
	cmd := exec.Command("hljs", "-l", "html")
	cmd.Stdin = bytes.NewBuffer([]byte(input))
	output, err := cmd.CombinedOutput()
	if err != nil {
		panic(err.Error() + string(output))
	}
	return string(output)
}
