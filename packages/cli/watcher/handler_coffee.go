package watcher

import (
	"github.com/teacat/pathx"
	"github.com/tocas-ui/tocas-ui/cli/path"
)

func coffee(e *event) {
	executeWithEvent(e, []string{"coffee", "-b", "--output", pathx.Join(path.PathModulesDist, e.name+".js"), "--compile", e.path})
}
