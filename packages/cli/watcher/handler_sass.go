package watcher

import (
	"github.com/tocas-ui/tocas-ui/cli/config"
	"github.com/tocas-ui/tocas-ui/cli/path"
)

func sass(e *event) {
	switch config.Int(config.KeySassCompiler) {
	case config.CompilerSass:
		executeWithEvent(e, []string{"sass", "--indented", path.PathComponentsSrcTocas + ":" + path.PathComponentsDistTocas})
	case config.CompilerWellington:
		executeWithEvent(e, []string{"wt", "compile", path.PathComponentsSrcTocas, "-b", path.PathComponentsDist})
	case config.CompilerNode:
		executeWithEvent(e, []string{"node-sass", path.PathComponentsSrcTocas, ">", path.PathComponentsDistTocas})
	case config.CompilerSassc:
		executeWithEvent(e, []string{"sassc", "--sass", path.PathComponentsSrcTocas, path.PathComponentsDistTocas})
	}
}
