package docs

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/iancoleman/strcase"

	"github.com/qor/i18n"
	"github.com/qor/i18n/backends/yaml"
	"github.com/teacat/pathx"
	"github.com/tocas-ui/tocas-ui/cli/path"
	"github.com/tocas-ui/tocas-ui/cli/util"
	blackfriday "gopkg.in/russross/blackfriday.v2"
	yamlg "gopkg.in/yaml.v2"
)

var locales map[string]*localeInfo

type localeInfo struct {
	name         string
	locale       string
	path         string
	i18n         *i18n.I18n
	links        []*DocumentLink
	contributors []*DocumentContributor
}

func markdown(s string) string {
	if strings.Contains(s, "[") || strings.Contains(s, "*") || strings.Contains(s, "`") {
		s = string(blackfriday.Run([]byte(s)))
	}
	return s
}

func (l *localeInfo) u(key string, args ...interface{}) string {
	return markdown(string(l.i18n.T(l.locale, "UI."+key, args...)))
}

func (l *localeInfo) m(key string, args ...interface{}) string {
	return markdown(string(l.i18n.T(l.locale, key, args...)))
}

func (l *localeInfo) l(key string, args ...interface{}) string {
	return markdown(string(l.i18n.T(l.locale, "Links."+key, args...)))
}

func (l *localeInfo) t(key string, args ...interface{}) string {
	return markdown(string(l.i18n.T(l.locale, "Types."+key, args...)))
}

const (
	LinkTypeComponents = iota
	LinkTypeWidgets
	LinkTypeGettingStarteds
)

func (l *localeInfo) generateLinks(typ int) *DocumentLink {
	var items []*LinkItem
	var names []string
	var name string
	var path string
	switch typ {
	case LinkTypeComponents:
		name = "Components"
		path = "components"
		names = util.ComponentNames()
		break
	case LinkTypeWidgets:
		name = "Widgets"
		path = "widgets"
		names = util.WidgetNames()
		break
	case LinkTypeGettingStarteds:
		name = "GettingStarted"
		path = "getting-started"
		names = []string{"introduction", "responsive", "typography"}
		break
	}
	for _, v := range names {
		anchor := strcase.ToKebab(v)
		camelName := strcase.ToCamel(v)
		items = append(items, &LinkItem{
			Name:  l.l(camelName),
			Alias: camelName,
			URL:   fmt.Sprintf("../%s/%s.html", path, anchor),
		})
	}
	return &DocumentLink{
		Title: l.l(name),
		Items: items,
	}
}

func (l *localeInfo) loadLinks() {
	l.links = []*DocumentLink{l.generateLinks(LinkTypeGettingStarteds), l.generateLinks(LinkTypeComponents), l.generateLinks(LinkTypeWidgets)}
}

func getLocales() map[string]string {
	l := make(map[string]string)
	for k, v := range locales {
		l[k] = v.name
	}
	return l
}

func loadLocales() {

	locales = make(map[string]*localeInfo)

	files, err := filepath.Glob(pathx.Join(path.PathDocsTranslations, "*"))
	if err != nil {
		panic(err)
	}
	for _, v := range files {

		locale := pathx.Name(v)
		//
		b, err := ioutil.ReadFile(pathx.Join(v, locale+".yml"))
		if err != nil {
			panic(err)
		}
		//
		var c map[string]interface{}
		err = yamlg.Unmarshal(b, &c)
		if err != nil {
			panic(err)
		}
		//
		i := i18n.New(
			yaml.New(pathx.JoinDir(path.PathDocsTranslations, locale)),
		)
		//
		l := &localeInfo{
			name:   c[locale].(map[interface{}]interface{})["Language"].(string),
			locale: locale,
			i18n:   i,
		}
		l.loadLinks()

		var ctrs []*DocumentContributor
		for _, v := range c[locale].(map[interface{}]interface{})["Contributors"].([]interface{}) {
			a := v.(map[interface{}]interface{})
			c := &DocumentContributor{
				Name:    a["Name"].(string),
				Website: a["Website"].(string),
			}
			ctrs = append(ctrs, c)
		}
		fmt.Println(ctrs)
		l.contributors = ctrs

		locales[locale] = l

	}
}
