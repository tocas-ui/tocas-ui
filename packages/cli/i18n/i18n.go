package i18n

import (
	"io/ioutil"
	"path/filepath"

	"github.com/qor/i18n"
	"github.com/qor/i18n/backends/yaml"
	"github.com/teacat/pathx"
	"github.com/tocas-ui/tocas-ui/cli/config"
	"github.com/tocas-ui/tocas-ui/cli/path"
	yamlg "gopkg.in/yaml.v2"
)

// i 呈現了目前終端指令列的語系包覆物件。
var i *wrapper

// wrapper 包覆語系與其資訊。
type wrapper struct {
	locale string
	i18n   *i18n.I18n
}

// Init 會以目前的語系設定初始化語系系統。
func Init() {
	i = &wrapper{
		locale: config.String(config.KeyLocale),
		i18n: i18n.New(
			yaml.New(path.PathCliTranslations),
		),
	}
}

// SetLocale 會更改目前的語系並寫入設定。
func SetLocale(locale string) {
	i.locale = locale
	config.Set(config.KeyLocale, locale)
}

// M 能夠取得語系中的 `Messages` 訊息字串。
func M(key string, args ...interface{}) string {
	return string(i.i18n.T(i.locale, "Messages."+key, args...))
}

// O 能夠取得語系中 `Options` 選項字串。
func O(key string, args ...interface{}) string {
	return string(i.i18n.T(i.locale, "Options."+key, args...))
}

// Q 能夠取得語系中 `Questions` 問題字串。
func Q(key string, args ...interface{}) string {
	return string(i.i18n.T(i.locale, "Questions."+key, args...))
}

//
func Locale() string {
	return i.locale
}

// Locales 會回傳所有可用的語系與其顯示名稱。
func Locales() map[string]string {
	// 取得語系翻譯底下的所有多國語系檔案。
	locales := make(map[string]string)
	files, err := filepath.Glob(pathx.Join(path.PathCliTranslations, "*"))
	if err != nil {
		panic(err)
	}
	for _, v := range files {
		locale := pathx.Name(v)

		// 讀取該語系檔案，並且取得該檔案中的語系顯示名稱字串。
		b, err := ioutil.ReadFile(v)
		if err != nil {
			panic(err)
		}
		var c map[string]interface{}
		err = yamlg.Unmarshal(b, &c)
		if err != nil {
			panic(err)
		}
		locales[locale] = c[locale].(map[interface{}]interface{})["Language"].(string)
	}
	return locales
}
