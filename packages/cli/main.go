package main

import (
	"github.com/tocas-ui/tocas-ui/cli/config"
	"github.com/tocas-ui/tocas-ui/cli/docs"
	"github.com/tocas-ui/tocas-ui/cli/i18n"
	"github.com/tocas-ui/tocas-ui/cli/interact"
)

func main() {
	config.Init()
	i18n.Init()
	docs.Init()
	interact.Start()
}
