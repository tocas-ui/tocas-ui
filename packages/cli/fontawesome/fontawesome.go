package fontawesome

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/google/go-github/github"
	"github.com/mholt/archiver"
	"github.com/teacat/pathx"
	"github.com/tocas-ui/tocas-ui/cli/path"
)

var (
	release     *github.RepositoryRelease
	releaseName string
	file        *os.File
	newContent  string
)

type Icon struct {
	Changes []string `json:"changes"`
	Label   string   `json:"label"`
	Styles  []string `json:"styles"`
	Unicode string   `json:"unicode"`
}

//
func Start() {
	//
	fetch()
	//
	download()
	//
	copy()
	//
	parse()
	//
	rewrite()
}

//
func fetch() {
	//
	client := github.NewClient(nil)
	//
	r, _, err := client.Repositories.GetLatestRelease(context.Background(), "FortAwesome", "Font-Awesome")
	if err != nil {
		panic(err)
	}
	release = r
	return
}

//
func download() {
	var url string
	var err error
	//
	for _, asset := range release.Assets {
		if strings.HasSuffix(asset.GetBrowserDownloadURL(), "web.zip") {
			url = asset.GetBrowserDownloadURL()
			break
		}
		continue
	}
	if url == "" {
		panic(errors.New("no web font was found in the repo"))
	}

	//
	releaseName = pathx.Name(url)
	//
	if _, err := os.Stat(pathx.Join(os.TempDir(), releaseName)); !os.IsNotExist(err) {
		log.Printf("已在本地找到最新的 Font Awesome：%s", releaseName)
		return
	}
	//
	file, err = ioutil.TempFile(os.TempDir(), releaseName)
	if err != nil {
		panic(err)
	}
	//
	log.Printf("正在下載最新的 Font Awesome：%s", url)
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	//
	n, err := io.Copy(file, resp.Body)
	if err != nil {
		panic(err)
	}
	log.Printf("已經複製 Font Awesome 整體套件（%d Bytes）", n)
	//
	log.Printf("正在解壓縮套件…")
	err = archiver.Zip.Open(file.Name(), os.TempDir())
	if err != nil {
		panic(err)
	}
	return
}

//
func copy() {
	//
	log.Printf("正在將最新的圖示字體複製至 Tocas UI 資料夾…")
	fontsPath, err := filepath.Glob(pathx.Join(os.TempDir(), releaseName, "webfonts", "*"))
	if err != nil {
		panic(err)
	}
	//
	for _, v := range fontsPath {
		dat, err := ioutil.ReadFile(v)
		if err != nil {
			panic(err)
		}
		err = os.MkdirAll(path.PathComponentsDistIcons, os.ModePerm)
		if err != nil {
			panic(err)
		}
		err = ioutil.WriteFile(pathx.Join(path.PathComponentsDistIcons, filepath.Base(v)), dat, os.ModePerm)
		if err != nil {
			panic(err)
		}
		log.Printf("已複製 %s", v)
	}
}

//
func parse() {
	//
	log.Printf("正在轉譯 Font Awesome 圖示至 Tocas UI 格式…")
	iconList, err := ioutil.ReadFile(pathx.Join(os.TempDir(), releaseName, "metadata", "icons.json"))
	if err != nil {
		panic(err)
	}

	//
	var icons map[string]Icon
	err = json.Unmarshal(iconList, &icons)
	if err != nil {
		panic(err)
	}

	// newContent 是新的 Tocas 圖示原始碼內容，會取代舊有機器產生的圖示原始碼。

	// 遞迴每個圖示。
	for k, v := range icons {
		// 將新圖示中的每個分隔符號，轉換成另一個 CSS 類別名稱，
		// 因為這樣才能符合 Tocas 的命名方式（如：`user-add` 變成 `user add`）。
		className := strings.Replace(k, "-", ".", -1)
		// 將別名的 `alt` 縮寫改為正統的 `alternate` 名字。
		className = strings.Replace(k, ".alt.", ".alternate.", -1)
		// 如果類別名稱比較特殊就脫逸字元作為暫時解法。
		if className == "500px" {
			className = "\\35 00px"
		}
		// 替這次修改的樣式名稱建立一個 CSS 樣式選擇器。
		selector := fmt.Sprintf("i.%s.icon:before", className)

		// 將這個圖示組合成一個 Sass 語法，稍會會收集所有語法且一次存入 Tocas 圖示原始碼中。
		newContent += fmt.Sprintf("%s\n    ", selector)
		// 如果這個圖示是 Logo 商標的話，那麼就額外追加商標的樣式。
		if v.Styles[0] == "brands" {
			newContent += fmt.Sprintf("+extend(brands)\n    ")
		}
		newContent += fmt.Sprintf("content: \"\\%s\"\n", v.Unicode)
	}
	return
}

//
func rewrite() {
	dat, err := ioutil.ReadFile(path.PathComponentsIcon)
	if err != nil {
		panic(err)
	}

	// 找出機器自動修改的原始碼區段，並且更改其中的圖示原始碼為這次新版本的圖示原始碼。
	re, err := regexp.Compile(`(\/\/ DO NOT EDIT AFTER THIS LINE \(不要編輯此行之後的樣式\))((.|\n)*)(\/\/ DO NOT EDIT BEFORE THIS LINE \(不要編輯此行之前的樣式\))`)
	if err != nil {
		panic(err)
	}
	newContent = re.ReplaceAllString(string(dat), fmt.Sprintf("$1\n%s$4", newContent))

	// 找出機器自動修改的版本號行列，並且更新其版本號。
	re, err = regexp.Compile(`\/\/ DO NOT EDIT THIS LINE \(不要編輯此行\):.*?\n((.|\n)*)`)
	if err != nil {
		panic(err)
	}
	newContent = re.ReplaceAllString(string(dat), fmt.Sprintf("// DO NOT EDIT THIS LINE (不要編輯此行): %s\n$1", releaseName))

	// 將機器自動修改後的結果存入 Tocas 的圖示原始碼內，完成本次的自動升級。
	err = ioutil.WriteFile(path.PathComponentsIcon, []byte(newContent), 777)
	if err != nil {
		panic(err)
	}
	log.Printf("已將 Font Awesome 圖示轉譯並存入 Tocas UI 原始碼")
}
