class Comparison

    # --------------------------------------------------------------
    # 公開方法
    # --------------------------------------------------------------

    showAfter: =>
    showBefore: =>
    setResizer: (percent) =>
    setAfterURL: (url) =>
    setBeforeURL: (url) =>