class Checkbox

    # --------------------------------------------------------------
    # 公開方法
    # --------------------------------------------------------------

    toggle: =>
    check: =>
    uncheck: =>
    uncheckAll: =>
    disable: =>
    enable: =>
    isEnable: =>
    isDisable: =>
    isChecked: =>
    isUnchecked: =>