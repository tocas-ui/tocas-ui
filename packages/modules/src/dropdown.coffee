class Dropdown

    # --------------------------------------------------------------
    # 公開方法
    # --------------------------------------------------------------

    setupMenu: (values) =>

    changeValues: (values) =>

    toggle: =>
    show: =>
    hide: =>
    clear: =>
    hideOthers: =>
    restoreDefaults: =>
    restoreDefaultText: =>
    restorePlaceholderText: =>
    restoreDefaultValue: =>
    saveDefaults: =>
    setSelected: (value) =>
    removeSelected: (value) ->
    setExactly: (values) =>
    setText: (text) =>
    setValue: (value) =>
    getText: =>
    getValue: =>
    getDefaultText: =>
    getPlaceholderText: =>
    getItem: (value) =>
    enable: =>
    disable: =>
    isSelection: =>
    isEnable: =>
    isDisable: =>

