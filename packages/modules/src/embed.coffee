class Embed

    # --------------------------------------------------------------
    # 公開方法
    # --------------------------------------------------------------

    change: (source, id, url) =>
    reset: =>
    show: =>
    hide: =>
    getID: =>
    getPlaceholder: =>
    getSources: =>
    getType: =>
    getURL: =>
    hasPlaceholder: =>
